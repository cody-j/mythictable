import axios from 'axios';

import { characters as defaultCharacters } from './defaultCharacterData';

const LibraryStore = {
    namespaced: true,
    state: {
        characters: defaultCharacters,
    },
    mutations: {
        updateCharacters(state, characters) {
            state.characters = [...defaultCharacters, ...characters];
        },
    },
    actions: {
        async addCharacter({ dispatch }, event) {
            let files;
            files = event && event.target && event.target.files;
            if (files && files.length) {
                const data = new FormData();
                data.append('files', files[0]);
                axios
                    .post('/api/files', data)
                    .then(() => {
                        dispatch('getCharacters');
                    })
                    .catch(err => {
                        console.error(err);
                    });
            }
        },
        async getCharacters({ commit }) {
            axios.get('/api/files').then(result => {
                const newCharacters = result.data.map(image => {
                    return {
                        id: image.id,
                        asset: {
                            kind: 'image',
                            src: image.url,
                        },
                    };
                });
                commit('updateCharacters', newCharacters);
            });
        },
    },
    getters: {
        characters: state => {
            return state.characters;
        },
    },
};

export default LibraryStore;
