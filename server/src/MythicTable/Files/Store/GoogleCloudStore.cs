using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MythicTable.Files.Data;

namespace MythicTable.Files.Store
{
    public class GoogleCloudStore : IFileStore
    {
        private readonly GoogleCredential googleCredential;
        private readonly StorageClient storageClient;
        private readonly string bucketName;

        public GoogleCloudStore(IConfiguration configuration)
        {
            string fileName = configuration.GetValue<string>("MTT_GCP_CREDENTIAL_FILE");
            googleCredential = string.IsNullOrEmpty(fileName) ?
                GoogleCredential.GetApplicationDefault() :
                GoogleCredential.FromFile(fileName);
            storageClient = StorageClient.Create(googleCredential);
            bucketName = configuration.GetValue<string>("MTT_GCP_BUCKET_IMAGES");
        }

        public async Task<FileDto> SaveFile(IFormFile formFile)
        {
            string fileNameForStorage = null;
            using (var memoryStream = new MemoryStream())
            {
                await formFile.CopyToAsync(memoryStream);
                var dataObject = await storageClient.UploadObjectAsync(bucketName, fileNameForStorage, null, memoryStream);
                return new FileDto
                {
                    Path = dataObject.Id,
                    Url = dataObject.MediaLink
                };
            }
        }

        public async Task DeleteFiles(List<FileDto> files)
        {
            var tasks = new List<Task>();
            foreach (var file in files)
            {
                tasks.Add(storageClient.DeleteObjectAsync(bucketName, file.Url));
            }
            await Task.WhenAll(tasks);
        }
    }
}
