﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MythicTable.Files.Data
{
    public interface IFileOwnershipProvider
    {
        Task<List<FileDto>> GetAll(string userId);
        Task<FileDto> Get(string id, string userId);
        Task<FileDto> Create(string path, string userId);
        Task<FileDto> Delete(string id, string userId);
        Task<FileDto> Store(FileDto dto);
    }
}
