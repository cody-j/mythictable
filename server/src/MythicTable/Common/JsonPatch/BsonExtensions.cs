﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Dynamic;

namespace MythicTable.Util.JsonPatch
{
    public static class BsonExtensions
    {
        public static JObject AsJson(this BsonDocument doc)
        {
            return JObject.Parse(doc.ToJson());
        }

        public static ExpandoObject AsExpandoObject(this BsonDocument doc)
        {
            return JsonConvert.DeserializeObject<ExpandoObject>(doc.ToJson());
        }

        public static BsonDocument AsBson(this JObject json)
        {
            return BsonDocument.Parse(json.ToString());
        }

        public static BsonDocument AsBson(dynamic obj)
        {
            return BsonDocument.Parse(JsonConvert.SerializeObject(obj));
        }

        public static BsonDocument Patch(this BsonDocument doc, JsonPatchDocument patch)
        {
            var obj = doc.AsExpandoObject();
            patch.ApplyTo(obj);
            return AsBson(obj);
        }

        public static BsonDocument Patch(this BsonDocument doc, Operation operation)
        {
            var obj = doc.AsExpandoObject();
            var patch = new JsonPatchDocument();
            patch.Operations.Add(operation);
            patch.ApplyTo(obj);
            return AsBson(obj);
        }
    }
}
