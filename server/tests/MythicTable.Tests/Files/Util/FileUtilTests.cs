using MythicTable.Files.Util;
using System;
using Xunit;

namespace MythicTable.Tests.Files.Util
{
    public class FileUtilTests
    {
        [Fact]
        public void TestCreatesPathFromDate()
        {
            Assert.Equal("2020/04/30/", FileUtil.CreatePath(new DateTime(2020, 4, 30)));
        }
    }
}
