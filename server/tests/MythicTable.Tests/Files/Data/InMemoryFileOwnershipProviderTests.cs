using Microsoft.AspNetCore.Http;
using Moq;
using MythicTable.Files.Data;
using MythicTable.Files.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MythicTable.Tests.Files.Data
{
    public class InMemoryFileOwnershipProviderTests
    {
        public IFileOwnershipProvider provider;
        public InMemoryFileOwnershipProviderTests()
        {
            provider = new InMemoryFileOwnershipProvider();
        }

        [Fact]
        public async Task TestCreateAndGet()
        {
            var createdDto = await provider.Create("file", "user");
            var fetchedDto = await provider.Get(createdDto.Id, "user");
            Assert.Equal(createdDto, fetchedDto);
        }

        [Fact]
        public async Task TestGetNonExistentThrows()
        {
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Get("1", "user"));
            Assert.Equal("Could not find File of Id: '1'", exception.Message);
        }

        [Fact]
        public async Task TestGetWithWrongUserThrows()
        {
            var createdDto = await provider.Create("file", "user");
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Get(createdDto.Id, "user2"));
            Assert.Equal("File '1' does not belong to user 'user2'", exception.Message);
        }

        [Fact]
        public async Task TestDeleteNonExistentThrows()
        {
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Delete("1", "user"));
            Assert.Equal("Could not find File of Id: '1'", exception.Message);
        }

        [Fact]
        public async Task TestDeleteWithWrongUserThrows()
        {
            var createdDto = await provider.Create("file", "user");
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Delete(createdDto.Id, "user2"));
            Assert.Equal("File '1' does not belong to user 'user2'", exception.Message);
        }

        [Fact]
        public async Task TestDeleteRemovesTheFile()
        {
            var createdDto = await provider.Create("file", "user");
            await provider.Delete(createdDto.Id, "user");
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Get(createdDto.Id, "user"));
            Assert.Equal("Could not find File of Id: '1'", exception.Message);
        }

        [Fact]
        public async Task TestCreateAndGetAll()
        {
            await provider.Create("file1", "user");
            await provider.Create("file2", "user");
            var all = await provider.GetAll("user");
            Assert.Equal(2, all.Count);
        }

        [Fact]
        public async Task TestStoreAddsId()
        {
            var dto = new FileDto
            {
                Path = "",
                User = "user"
            };
            Assert.Null(dto.Id);
            dto = await provider.Store(dto);
            Assert.NotNull(dto.Id);
        }
    }
}
