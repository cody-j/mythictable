using MongoDB.Bson;
using MythicTable.Character.Data;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Tests.Character.Data
{
    public class CharacterPatchTranslatorTests
    {
        private readonly CharacterPatchTranslator translator;

        public CharacterPatchTranslatorTests()
        {
            translator = new CharacterPatchTranslator();
        }

        [Fact]
        public void TestJsonPath2MongoPathForTokens()
        {
            var mongoPath = translator.JsonPath2MongoPath("/token/pos");
            Assert.Equal("Characters.$.Token.pos", mongoPath);
        }

        [Fact]
        public void TestJsonPath2MongoPathHandlesTrailingSlash()
        {
            var mongoPath = translator.JsonPath2MongoPath("/token/pos/");
            Assert.Equal("Characters.$.Token.pos", mongoPath);
        }

        [Fact]
        public void TestJsonPath2MongoPathForAttributes()
        {
            var mongoPath = translator.JsonPath2MongoPath("/attributes.description");
            Assert.Equal("Characters.$.Attributes.description", mongoPath);
        }

        [Fact]
        public void TestJsonPath2MongoPathHandlesKeywordsInPath()
        {
            var mongoPath = translator.JsonPath2MongoPath("/token/pos/token");
            Assert.Equal("Characters.$.Token.pos.token", mongoPath);
            mongoPath = translator.JsonPath2MongoPath("/token/pos/attributes");
            Assert.Equal("Characters.$.Token.pos.attributes", mongoPath);
        }

        [Fact]
        public void TestJsonPath2MongoPathForAssets()
        {
            var mongoPath = translator.JsonPath2MongoPath("/asset/src");
            Assert.Equal("Characters.$.Asset.src", mongoPath);
        }

        [Fact]
        public void TestJObjectToBson()
        {
            var value = translator.Json2Mongo(new JObject { { "q", 5 }, { "r", 6 } });
            Assert.Equal(new BsonDocument { { "q", 5 }, { "r", 6 } }, value);
        }

        [Fact]
        public void TestJsonValueToBson()
        {
            var value = translator.Json2Mongo(new JValue("string"));
            Assert.IsType<string>(value);
            Assert.Equal("string", value);
        }
    }
}